﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimator : MonoBehaviour
{

    [SerializeField] private Transform rotationTransform;
    [SerializeField] private Transform positionTransform;
    [SerializeField] private new Camera camera;

    [SerializeField] private AnimationCurve animCurve;

    private CameraModel cameraSettings;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FovProcess());
        StartCoroutine(RoamingProcess());
    }

    // Update is called once per frame
    void Update()
    {
        if (cameraSettings != GameManager.Data.cameraSettings)
        {
            cameraSettings = GameManager.Data.cameraSettings;
            positionTransform.localPosition = new Vector3(0, cameraSettings.height, -cameraSettings.roundRadius);
        }

        rotationTransform.Rotate(new Vector3(0, 360, 0) * Time.deltaTime / cameraSettings.roundDuration);
    }

    private void LateUpdate()
    {
        camera.transform.LookAt(new Vector3(0, cameraSettings.lookAtHeight, 0), Vector3.up);
    }

    private IEnumerator FovProcess()
    {
        while (true)
        {
            if (cameraSettings == null) yield return null;

            float startFov = camera.fieldOfView;
            float targetFov = Random.Range(cameraSettings.fovMin, cameraSettings.fovMax);

            float timer = 0;
            while (timer < cameraSettings.fovDuration)
            {
                timer += Time.deltaTime;
                float progress = animCurve.Evaluate(timer / cameraSettings.fovDuration);
                camera.fieldOfView = Mathf.Lerp(startFov, targetFov, progress);
                yield return null;
            }

            yield return new WaitForSeconds(cameraSettings.fovDelay);
        }
    }


    private IEnumerator RoamingProcess()
    {
        while (true)
        {
            if (cameraSettings == null) yield return null;

            Vector3 startPosition = camera.transform.localPosition;
            Vector3 nextPosition = Random.insideUnitSphere * cameraSettings.roamingRadius;
            float distance = Vector3.Distance(startPosition, nextPosition);
            float duration = distance * cameraSettings.roamingDuration;

            float timer = 0;
            while (timer < duration)
            {
                timer += Time.deltaTime;
                float progress = animCurve.Evaluate(timer / duration);
                camera.transform.localPosition = Vector3.Lerp(startPosition, nextPosition, progress);
                yield return null;
            }
        }
    }
}
