﻿using UnityEngine;
using System.Collections;
using BroTools;
using TMPro;

public class HPBar : MonoBehaviour
{
    public BroTweener hideTweener;

    [SerializeField] private SpriteRenderer _foreground;
    [SerializeField] private float _width;
    [SerializeField] private float _factor;

    [SerializeField, Range(0, 1f)]
    private float _value;

    [SerializeField] private TextMeshPro labelPrefab;

    private ObjectPool<TextMeshPro> labelPool = new ObjectPool<TextMeshPro>();

    public float Value
    {
        get { return _value; }
        set
        {
            float xScale = Mathf.Lerp(0, _width * _factor, value);
            float xPos = Mathf.Lerp(-_width / 2, 0, value);
            _foreground.transform.localScale = new Vector3(xScale, _foreground.transform.localScale.y, _foreground.transform.localScale.z);
            _foreground.transform.localPosition = new Vector3(xPos, _foreground.transform.localPosition.y);
            _value = value;
        }
    }

    public void Set(float current, float max)
    {
        Value = current / max;
    }

    void OnValidate()
    {
        if (!_foreground) return;
        Value = _value;
    }


    public void PlayLabel(float value, Color color)
    {
        var label = labelPool.Get(labelPrefab, transform.parent);
        label.color = color;

        if (value > 0)
        {
            label.text = $"+{Mathf.Round(value)}";
        } else
        {
            label.text = $"{Mathf.Round(value)}";
        }

        label.transform.position = labelPrefab.transform.position;
        label.gameObject.SetActive(true);
        label.GetComponent<BroTweener>().SetOnFinish(() => labelPool.Return(label, true));
    }
}
