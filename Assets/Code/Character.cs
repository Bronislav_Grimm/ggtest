﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
    public CharacterData data;
    public Animator animator;
    public HPBar hpBar;
    public UIPlayerPanel panel;

    public void Set(CharacterData data)
    {
        this.data = data;

        hpBar.hideTweener.SetProgress(0);
        hpBar.Set(data.stats[StatsId.LIFE_ID].Value, data.stats[StatsId.LIFE_ID].MaxValue);
        animator.SetInteger("Health", (int)data.stats[StatsId.LIFE_ID].Value);
        panel.Set(data);
    }
}
