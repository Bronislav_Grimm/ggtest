﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterData
{
    public Dictionary<int, StatState> stats;

    public List<int> buffs;

    public event System.Action OnUpdated;

    public bool IsDead => stats[StatsId.LIFE_ID].Value <= 0;

    public CharacterData(Data data)
    {
        stats = new Dictionary<int, StatState>();
        buffs = new List<int>();

        foreach (Stat stat in data.stats)
        {
            stats[stat.id] = new StatState(stat);
        }
    }


    public void SetRandomBuffs(Data data)
    {
        List<Buff> sourceBuffs = new List<Buff>(data.buffs);

        // Генерируем случайное количество баффов
        int count = Random.Range(data.settings.buffCountMin, data.settings.buffCountMax + 1);

        for (int i = 0; i < count; i++)
        {
            if (sourceBuffs.Count == 0) continue;

            // Генерируем случайный индекс баффа, и добавляем его в список
            int buffIndex = Random.Range(0, sourceBuffs.Count);
            Buff buff = sourceBuffs[buffIndex];

            buffs.Add(buff.id);

            // Применяем модификаторы баффа
            foreach (BuffStat buffStat in buff.stats)
            {
                if (stats.ContainsKey(buffStat.statId))
                {
                    stats[buffStat.statId].MaxValue += buffStat.value;
                    stats[buffStat.statId].ResetValue();
                }
            }

            // Если не должно быть дубликатов, удаляем использованный бафф из начального списка
            if (!data.settings.allowDuplicateBuffs)
            {
                sourceBuffs.RemoveAt(buffIndex);
            }
        }
    }

    // Возвращает фактически отнятое здоровье
    public float Damage(float value)
    {
        if (value > stats[StatsId.LIFE_ID].Value)
        {
            value = stats[StatsId.LIFE_ID].Value;
        }

        stats[StatsId.LIFE_ID].Value -= value;

        OnUpdated?.Invoke();

        return value;
    }

    // Возвращает фактически добавленное здоровье
    public float Heal(float value)
    {
        if (stats[StatsId.LIFE_ID].Value + value > stats[StatsId.LIFE_ID].MaxValue)
        {
            value = stats[StatsId.LIFE_ID].MaxValue - stats[StatsId.LIFE_ID].Value;
        }

        stats[StatsId.LIFE_ID].Value += value;

        OnUpdated?.Invoke();

        return value;
    }
}


public class StatState
{
    public StatState(Stat config)
    {
        StatConfig = config;
        MaxValue = config.value;
        ResetValue();
    }

    public Stat StatConfig { get; }

    public float Value;
    public float MaxValue;

    public void ResetValue()
    {
        Value = MaxValue;
    }
}
