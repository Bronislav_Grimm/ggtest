﻿using UnityEngine;
using UnityEngine.UI;

public class UIStat : MonoBehaviour
{
    public Image icon;
    public Text text;

    public void SetIcon(string iconName)
    {
        icon.sprite = Resources.Load<Sprite>($"Icons/{iconName}");
    }

    public void SetText(string value)
    {
        text.text = value;
    }
}
