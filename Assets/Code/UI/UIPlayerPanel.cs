﻿using UnityEngine;
using UnityEngine.UI;

public class UIPlayerPanel : MonoBehaviour
{
    public static event System.Action<CharacterData> OnCharacterAttack;

    [SerializeField] private Transform statsPanel;
    [SerializeField] private Transform buffsPanel;

    [SerializeField] private UIStat statPrefab;
    [SerializeField] private UIStat buffPrefab;

    private BroTools.DictionaryPoolable<object, UIStat> statsPool = new BroTools.DictionaryPoolable<object, UIStat>();
    private BroTools.ObjectPool<UIStat> buffsPool = new BroTools.ObjectPool<UIStat>();
    private CharacterData charData;


    public void Set(CharacterData charData)
    {
        this.charData = charData;

        charData.OnUpdated += OnDataUpdated_Handler;

        statsPool.Clear();
        buffsPool.ReturnAll(true);

        foreach (var keyValuePair in charData.stats)
        {
            StatState stat = keyValuePair.Value;

            UIStat uiStat = statsPool.Add(stat.StatConfig, statPrefab, statsPanel);

            uiStat.SetIcon(stat.StatConfig.icon);
            uiStat.SetText(stat.Value.ToString());

            uiStat.transform.SetAsLastSibling();
            uiStat.gameObject.SetActive(true);
        }


        foreach (var buffId in charData.buffs)
        {
            UIStat uiStat = buffsPool.Get(buffPrefab, buffsPanel);

            Buff buff = GameManager.Data.GetBuff(buffId);

            uiStat.SetIcon(buff.icon);
            uiStat.SetText(buff.title);

            uiStat.gameObject.SetActive(true);
        }
    }


    public void OnAttack()
    {
        OnCharacterAttack?.Invoke(charData);
    }

    public void OnDataUpdated_Handler()
    {
        foreach (var keyValuePair in charData.stats)
        {
            StatState stat = keyValuePair.Value;

            if (statsPool.ContainsKey(stat.StatConfig))
            {
                statsPool[stat.StatConfig].SetText(stat.Value.ToString());
            }
        }
    }
}
