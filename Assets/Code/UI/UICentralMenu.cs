﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICentralMenu : MonoBehaviour
{
    public static event Action<bool> OnNewGame;

    public void NewGameBase()
    {
        OnNewGame?.Invoke(false);
    }

    public void NewGameWithBuffs()
    {
        OnNewGame?.Invoke(true);
    }
}
