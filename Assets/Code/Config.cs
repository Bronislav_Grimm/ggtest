﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config : MonoBehaviour
{
    public static Data GetData()
    {
        TextAsset jsonAsset = Resources.Load<TextAsset>("data");
        Data data = JsonUtility.FromJson<Data>(jsonAsset.text);

        return data;
    }
}
