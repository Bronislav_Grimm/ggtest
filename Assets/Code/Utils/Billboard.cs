﻿using UnityEngine;
using System.Collections;

namespace GameCore
{
    [ExecuteAlways]
    public class Billboard : MonoBehaviour
    {
        [SerializeField] private bool debug;

        private new Camera camera;

        private void LateUpdate()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying && !debug) return;
#endif
            if (!camera) camera = Camera.main;

            transform.LookAt(transform.position + camera.transform.forward);
        }
    }
}