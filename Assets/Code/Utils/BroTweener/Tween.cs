﻿using UnityEngine;
using System.Collections;

namespace BroTools
{

    public enum Tween
    {
        Position,
        Rotation,
        Scale,
        Width,
        Height,
        Color,
        Alpha,
        Link,
        Action,
    }
}