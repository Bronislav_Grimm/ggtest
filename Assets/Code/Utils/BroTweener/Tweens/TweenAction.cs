﻿using UnityEngine;
using System.Collections;

namespace BroTools
{

    public class TweenAction : ITween
    {
        private TweenData.ProcessEvent action;

        public void ApplyValue(Transform target, TweenData data, float value)
        {
            data.processEvent?.Invoke(value);
        }
    }
}