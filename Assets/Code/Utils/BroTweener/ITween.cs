﻿using UnityEngine;
using System.Collections;

namespace BroTools
{

    public interface ITween
    {
        void ApplyValue(Transform target, TweenData data, float value);
    }
}