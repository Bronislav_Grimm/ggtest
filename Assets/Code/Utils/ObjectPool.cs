﻿using System.Collections.Generic;
using UnityEngine;

namespace BroTools
{
    /// <summary>
    /// Пул объектов
    /// </summary>
    public class ObjectPool<T> where T : Component
    {
        /// <summary>
        /// Словарь пулов: префаб, пул его экземпляров
        /// </summary>
        private Dictionary<T, Stack<T>> pool = new Dictionary<T, Stack<T>>();

        /// <summary>
        /// Словарь экземпляров взятых из пула
        /// Key - экземпляр префаба, value - оригинальный префаб
        /// </summary>
        private Dictionary<T, T> usingObjects = new Dictionary<T, T>();

        /// <summary>
        /// Наполнить пул объектами до заданного количества
        /// </summary>
        public void FillPool(T prefab, int objectsCount, Transform parent = null)
        {
            if (!pool.ContainsKey(prefab))
            {
                pool[prefab] = new Stack<T>();
            }

            for (int i = pool[prefab].Count; i < objectsCount; i++)
            {
                pool[prefab].Push(Object.Instantiate(prefab, parent));
            }
        }

        /// <summary>
        /// Получить из пула объект, если он есть,
        /// Иначе создать новый объект и получить его
        /// </summary>
        public T Get(T prefab, Transform parent = null)
        {
            if (!pool.ContainsKey(prefab))
            {
                pool[prefab] = new Stack<T>();
            }

            T instance = default;


            while (pool[prefab].Count > 0)
            {
                // Вынимаем нужный экземпляр из стека
                instance = pool[prefab].Pop();

                // Если сохранённый экземпляр существует, выходим из цикла
                if (instance)
                {
                    if (parent)
                    {
                        instance.transform.SetParent(parent, false);
                    }
                    break;
                }

                // Если экземпляр был потерян (например при переходе между сценами), удаляем его из используемых объектов 
                if (usingObjects.ContainsKey(instance)) usingObjects.Remove(instance);
            }

            if (!instance) instance = Object.Instantiate(prefab, parent);

            usingObjects.Add(instance, prefab);

            return instance;
        }

        /// <summary>
        /// Вернуть объект в пул
        /// </summary>
        public void Return(T obj, bool deactivate = false)
        {
            if (usingObjects.TryGetValue(obj, out T originPrefab))
            {
                if (originPrefab)
                {
                    pool[originPrefab].Push(obj);
                }
                usingObjects.Remove(obj);

                if (deactivate && obj.gameObject.activeSelf)
                {
                    obj.gameObject.SetActive(false);
                }
            }
        }

        /// <summary>
        /// Вернуть все объекты в пул
        /// </summary>
        public void ReturnAll(bool deactivate = false)
        {
            foreach (T obj in usingObjects.Keys)
            {
                if (!obj) continue;

                pool[usingObjects[obj]].Push(obj);

                if (deactivate && obj.gameObject.activeSelf)
                {
                    obj.gameObject.SetActive(false);
                }
            }
            usingObjects.Clear();
        }
    }
}