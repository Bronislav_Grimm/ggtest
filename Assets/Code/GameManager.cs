﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public static Data Data;

    public Character[] characters = new Character[2];

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        UIPlayerPanel.OnCharacterAttack += OnAttack_Handler;
        UICentralMenu.OnNewGame += NewGame_Handler;
    }

    private void OnDisable()
    {
        UIPlayerPanel.OnCharacterAttack -= OnAttack_Handler;
        UICentralMenu.OnNewGame -= NewGame_Handler;
    }

    // Start is called before the first frame update
    void Start()
    {
        NewGame_Handler(true);
    }


    public void NewGame_Handler(bool withBuffs)
    {
        // Обновляю каждый раз, чтобы можно было менять json на лету
        Data = Config.GetData();

        // Не понял зачем в данных есть поле с количеством игроков
        for (int i = 0; i < characters.Length; i++)
        {
            CharacterData data = new CharacterData(Data);
            if (withBuffs) data.SetRandomBuffs(Data);

            characters[i].Set(data);
        }
    }

    public void OnAttack_Handler(CharacterData attacker)
    {
        Character sourceChar = null;
        Character targetChar = null;

        foreach (var character in characters)
        {
            if (character.data == attacker)
            {
                sourceChar = character;
            } else
            {
                targetChar = character;
            }
        }

        ApplyDamage(sourceChar, targetChar);
    }

    /// <summary>
    /// Обработка урона, защиты и вампиризма
    /// </summary>
    private void ApplyDamage(Character sourceChar, Character targetChar)
    {
        if (targetChar.data.IsDead || sourceChar.data.IsDead) return;

        sourceChar.animator.SetTrigger("Attack");


        float baseDamage = sourceChar.data.stats[StatsId.DAMAGE_ID].Value;
        float defense = targetChar.data.stats[StatsId.ARMOR_ID].Value;
        // Защита ограничена 100%
        defense = Mathf.Clamp(defense, 0, 100);
        float resultDamage = Mathf.Round(baseDamage / 100 * (100 - defense));

        float vampire = sourceChar.data.stats[StatsId.LIFE_STEAL_ID].Value;
        // Вампиризм ограничен 100%
        vampire = Mathf.Clamp(vampire, 0, 100);
        float stolenHp = Mathf.Round(resultDamage / 100 * vampire);

        // Применяем высчитанный урон к персонажу, 
        // получаем урон, который был нанесён, с учётом остававшегося у персонажа здоровья
        resultDamage = targetChar.data.Damage(resultDamage);

        if (resultDamage > 0)
        {
            targetChar.hpBar.Set(targetChar.data.stats[StatsId.LIFE_ID].Value, targetChar.data.stats[StatsId.LIFE_ID].MaxValue);
            targetChar.hpBar.PlayLabel(-resultDamage, Color.red);
            if (targetChar.data.IsDead) targetChar.hpBar.hideTweener.Play();
            targetChar.animator.SetInteger("Health", (int)targetChar.data.stats[StatsId.LIFE_ID].Value);
        }

        stolenHp = sourceChar.data.Heal(stolenHp);

        if (stolenHp > 0)
        {
            sourceChar.hpBar.Set(sourceChar.data.stats[StatsId.LIFE_ID].Value, sourceChar.data.stats[StatsId.LIFE_ID].MaxValue);
            sourceChar.hpBar.PlayLabel(stolenHp, Color.green);
            sourceChar.animator.SetInteger("Health", (int)sourceChar.data.stats[StatsId.LIFE_ID].Value);
        }
    }
}
